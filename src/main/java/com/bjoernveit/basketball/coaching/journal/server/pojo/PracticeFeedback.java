package com.bjoernveit.basketball.coaching.journal.server.pojo;


import lombok.Getter;
import lombok.Setter;

public class PracticeFeedback {

    @Getter @Setter
    private final boolean positive;
    @Getter @Setter
    private final String text;

    public PracticeFeedback(boolean positive, String text) {
        this.positive = positive;
        this.text = text;
    }
}
