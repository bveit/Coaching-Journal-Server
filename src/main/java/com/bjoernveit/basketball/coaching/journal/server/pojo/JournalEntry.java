package com.bjoernveit.basketball.coaching.journal.server.pojo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class JournalEntry {

    @Id
    public String id;
    @Getter @Setter
    private final String text;
    @Getter @Setter
    private final List<PracticeFeedback> feedbacks;
    @Getter @Setter
    private final List<String> labels;
    @Getter @Setter
    private final int nmbrOfPlayers;
    @Getter @Setter
    private final LocalDate date;
    @Getter @Setter
    private final String title;



    public JournalEntry(String text, List<PracticeFeedback> feedbacks, List<String> labels, int nmbrOfPlayers, LocalDate date, String title) {
        this.text = text;
        this.feedbacks = feedbacks;
        this.labels = labels;
        this.nmbrOfPlayers = nmbrOfPlayers;
        this.date = date;
        this.title = title;
    }


    public void addFeedback(PracticeFeedback feedback){
        feedbacks.add(feedback);
    }

    public void addAll(Collection<PracticeFeedback> feedbacks){
        this.feedbacks.addAll(feedbacks);
    }
}

